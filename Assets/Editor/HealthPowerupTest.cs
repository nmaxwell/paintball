﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using NSubstitute;

public class HealthPowerupTest
{

    [Test]
    public void TestCallsIncrementHealthOnCollisionWithPlayer()
    {
        var healthPowerup = new HealthPowerup();
        var collisionStub = Substitute.For<Collision>();
        var playerHealthStub = Substitute.For<PlayerHealth>();

        collisionStub.gameObject.GetComponent<PlayerHealth>().Returns(playerHealthStub);

        playerHealthStub.Received().IncrementHealth(healthPowerup.healthValue);
    }
}
