﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(SteamVR_TrackedController))]
public class ShootScript : MonoBehaviour
{
    public GameObject paintballPrefab;
    public float paintballVelocity;

    private SteamVR_TrackedController controller;
    private BroadcastPlayerHit broadcastPlayerHit;

    // Use this for initialization
    void Start ()
    {
        controller = GetComponent<SteamVR_TrackedController>();
        controller.TriggerClicked += new ClickedEventHandler(FirePaintball);
	}

    void Awake()
    {
        broadcastPlayerHit = transform.root.GetComponent<BroadcastPlayerHit>();
    }

    void FirePaintball(object sender, ClickedEventArgs e)
    {
        Quaternion rot = transform.rotation;
        GameObject paintball = (GameObject)Instantiate(paintballPrefab, transform.position, rot);
        paintball.GetComponent<Rigidbody>().AddForce(paintball.transform.forward * paintballVelocity);
        paintball.layer = LayerMask.NameToLayer("LocalPlayer");
        broadcastPlayerHit.CmdMakeRemotePaintball(paintball.transform.position, paintball.transform.rotation, paintballVelocity);
    }
}
