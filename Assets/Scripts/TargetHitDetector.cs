﻿using UnityEngine;
using System.Collections;

public class TargetHitDetector : MonoBehaviour
{
    public GameObject paintballSplatPrefab;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Paintball"))
        {
            //call event for collision happened
            GameManager.IncrementScore();
            //GameObject paintSplat = (GameObject)Instantiate(paintballSplatPrefab, collision.transform.position, collision.transform.rotation);
            //Destroy(paintSplat, 2.0f);
            //estroy(collision.gameObject);
        }
    }
}
