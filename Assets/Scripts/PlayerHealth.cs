﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour {

    private int health = 100;
    public int Health
    {
        get
        {
            return health;
        }
        set
        {
            health = value;
        }
    }

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
