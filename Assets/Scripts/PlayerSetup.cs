﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class PlayerSetup :  NetworkBehaviour
{
    [SerializeField]
    private Behaviour[] componentsToEnable;

    void Start()
    {
        if (isLocalPlayer)
        {
            foreach (Behaviour component in componentsToEnable)
            {
                component.enabled = true;
            }
            ChangeLayersRecursively(transform, "LocalPlayer");
        }
        else
        {
            ChangeLayersRecursively(transform, "RemotePlayer");
        }
    }

    void ChangeLayersRecursively(Transform parentTransform, string layer)
    {
        parentTransform.gameObject.layer = LayerMask.NameToLayer(layer);
        foreach (Transform childTransform in parentTransform)
        {
            ChangeLayersRecursively(childTransform, layer);
        }
    }
}